package com.ianpj.technicaltest.mcpayment;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Nomor3 {

	public static List<?> AnswerNo3(String word, int x) {

		try {

			ArrayList<String> arr = new ArrayList<String>();
			String[] arrOfStr = word.split(" ", -2);
			for (String data : arrOfStr) {
				if (data.length() == x) {
					arr.add(data);
				}
			}

			System.out.println("Result is " + arr);
			return arr;
		} catch (Exception e) {
			System.out.println(" Wrong input data !");
		}
		return null;
	}

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.print("Input word with space if > 1 word : ");
		String word = scan.nextLine();

		System.out.print("Input nilai x : ");
		int x = scan.nextInt();

		AnswerNo3(word, x);
		scan.close();
	}
}
