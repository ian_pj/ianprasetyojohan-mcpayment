package com.ianpj.technicaltest.mcpayment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Nomor2 {

	public static Set<?> AnswerNo2(int[] nums, int x) {

		try {

			Scanner scan = new Scanner(System.in);

			for (int i = 0; i < nums.length; i++) {
				System.out.print("Number ke-" + (i + 1) + ": ");
				nums[i] = scan.nextInt();
			}
			scan.close();

			System.out.println();
			System.out.println("===== your data =====");
			System.out.println("Nilai X is " + x);
			for (int data : nums) {
				System.out.print(data + "  ");
			}
			System.out.println();

			List<Integer> arrContainsResultX = new ArrayList<Integer>();
			HashMap<Integer, Integer> hashmap = new HashMap<Integer, Integer>();

			for (int i = 0; i < nums.length; i++) {
				for (int j = 0; j < nums.length; j++) {
					if ((nums[i] / nums[j]) == x) {
						arrContainsResultX.add(nums[i]);
					}
				}
				hashmap.put(nums[i], i);
			}

			boolean isAny = false;
			for (int i = 0; i < arrContainsResultX.size(); i++) {
				isAny = contains(nums, arrContainsResultX.get(i));
				if (isAny) {
					hashmap.remove(arrContainsResultX.get(i));
				}
			}

			System.out.println();
			System.out.println("Result is " + hashmap.keySet());

			return hashmap.keySet();
		} catch (Exception e) {
			System.out.println(" Wrong input data !");
		}
		return null;
	}

	public static boolean contains(final int[] arr, final int key) {
		return Arrays.stream(arr).anyMatch(i -> i == key);
	}

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.print("Input nilai x : ");
		int x = scan.nextInt();
		int[] nums = new int[4];

		AnswerNo2(nums, x);
		scan.close();
	}
}
