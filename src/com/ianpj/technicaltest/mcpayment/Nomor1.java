package com.ianpj.technicaltest.mcpayment;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class Nomor1 {

	public static Set<?> AnswerNo1 (int[] nums) {
		
		try {

			Scanner scan = new Scanner(System.in);

			for (int i = 0; i < nums.length; i++) {
				System.out.print("Number ke-" + (i + 1) + ": ");
				nums[i] = scan.nextInt();
			}
			scan.close();

			System.out.println();
			System.out.println("===== your data =====");
			for (int data : nums) {
				System.out.print(data + "  ");
			}
			System.out.println();

			HashMap<Integer,Integer> hashmap = new HashMap<Integer,Integer>();   
			int process = 0;
			
			for (int i = 0; i < nums.length; i++) {
				for (int j = i; j < nums.length; j++) {
					// System.out.print("data i "+nums[i] +" & data j "+nums[j] + " hasilnya : "+(nums[i] - nums[j]));
					if ((nums[i] - nums[j]) > 0) {
						process = nums[i];
					}
				}
				hashmap.put(process, i);   
			}
			
			System.out.println();
			System.out.println("Result is " + hashmap.keySet());

			return hashmap.keySet();
		} catch (Exception e) {
			System.out.println(" Wrong input data !");
		}
		return null;
	}

	public static void main(String[] args) {

		int[] nums = new int[4];
		AnswerNo1(nums);
	}
}
